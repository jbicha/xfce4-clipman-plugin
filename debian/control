Source: xfce4-clipman-plugin
Section: xfce
Priority: optional
Maintainer: Debian Xfce Maintainers <debian-xfce@lists.debian.org>
Uploaders: Yves-Alexis Perez <corsac@debian.org>,
           Lionel Le Folgoc <mrpouit@gmail.com>,
           Mike Massonnet <mmassonnet@gmail.com>
Build-Depends: autotools-dev,
               debhelper (>= 9),
               intltool,
               libexo-1-dev,
               libglade2-dev,
               libgtk-3-dev,
               libqrencode-dev,
               libunique-dev,
               libx11-dev,
               libxfce4panel-2.0-dev,
               libxfce4ui-2-dev,
               libxfce4util-dev,
               libxfconf-0-dev,
               libxml-parser-perl,
               libxml2-dev,
               libxtst-dev,
               pkg-config
Standards-Version: 4.2.1
Homepage: http://goodies.xfce.org/
Vcs-Git: https://salsa.debian.org/xfce-team/goodies/xfce4-clipman-plugin.git
Vcs-Browser: https://salsa.debian.org/xfce-team/goodies/xfce4-clipman-plugin

Package: xfce4-clipman
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: clipboard history utility
 Clipman enables you to store and recall X selections, as well as GTK+
 clipboard content. You can also define actions to be triggered by selecting
 some text pattern.
 .
 This package uses Xfce libraries but can be used without the Xfce panel
 and only requires a system tray (notification area).

Package: xfce4-clipman-plugin
Architecture: any
Depends: xfce4-clipman, ${misc:Depends}, ${shlibs:Depends}
Description: clipboard history plugin for Xfce panel
 Clipman enables you to store and recall X selections, as well as GTK+
 clipboard content. You can also define actions to be triggered by selecting
 some text pattern.
 .
 This package contains the Xfce panel plugin.
